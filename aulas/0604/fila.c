#include <stdlib.h>

#include "no.h"
#include "fila.h"

struct fila {
    struct no *prim;
    struct no *ult;
    int cont;
};


Fila *cria_fila()
{
    Fila *f = (Fila *) malloc(sizeof(Fila));
    f->prim = NULL;
    f->ult = NULL;
    f->cont = 0;
}

int fila_vazia(const Fila *f)
{
   /*  if ((f->cont == 0))
        return true;
    return false; */

    // return (f->cont == 0);

    return !f->cont;
}


int insere(int i, Fila *f)
{
    struct no *novo = cria_no(i);
    if(novo == NULL) {
        return 0;
    }
    if(fila_vazia(f)) {
        f->prim = novo;
    } else {
        f->ult->prox = novo;
    }
    f->ult = novo;
    f->cont++;
    return 1;
}


int remov(int *i, Fila *f)
{
    struct no *aux;
    if(fila_vazia(f)) {
        return 0;
    }
    aux = f->prim;
    *i = f->prim->info;
    f->prim= f->prim->prox;
    f->cont--;
    if(f->cont == 0) {
        f->ult == NULL;
    }
    free(aux);
    return 1;

}


int tamanho(const Fila *f)
{
    return f->cont;
}

void mostra_fila(const Fila *f)
{
    struct no * aux;
    if(fila_vazia(f)) {
        printf("fila vazia\n");
    } else {
        aux = f->prim;
        while(aux != NULL) {
            printf("%d ", aux->info);
            aux = aux->prox;
        }
        printf("\n");
    }
}