#include <stdio.h>
#include <time.h>
#include "fila.h"

int main(void)
{
    Fila *f = cria_fila();
    srand(time(0));
    int i;
    do {
        if(rand() % 3) {
            i = rand() % 10;
            // printf("%d \n", i);
            if(insere(i, f)) {
                printf("inseriu %d | ", i);
                mostra_fila(f);
                // mostra_vetor(f);
            }
        } else {
            // printf("r");
            if(remov(&i, f)) {
                printf("removeu %d | ", i);
                mostra_fila(f);
                // mostra_vetor(f);
            }
        }
    } while(!fila_vazia(f));
    return 0;
}