#include <stdio.h>
#include <stdlib.h>
#include "ponto.h"

struct ponto {
    float x, y;
};

Ponto * cria_ponto (float x, float y){
    Ponto * p = (Ponto *) malloc(sizeof(Ponto));
    if (p) {
        p->x = x;
        p->y = y;
    }
    return p;
}
void mostra_ponto (const Ponto *p, char * s){
    printf("\n%s(%.1f,%.1f)\n", s, p->x, p->y);
}