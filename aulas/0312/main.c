#include <stdio.h>
#include "ponto.h"

int main(int argc, char *argv[]){
    Ponto *p1, *p2;
    p1 = cria_ponto(0,0);
    p2 = cria_ponto(3,4);
    mostra_ponto(p1, "p1 = ");
    mostra_ponto(p2, "p2 = ");
    return 0;
}