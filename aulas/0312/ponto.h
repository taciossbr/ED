#ifndef _PONTO_H_
typedef struct ponto Ponto;

Ponto * cria_ponto (float, float);

void mostra_ponto (const Ponto *, char *);

void libera_ponto (Ponto *);

void consulta_ponto (Ponto *, const float *, const float *);

float distancia (const Ponto *, const Ponto *);

#endif