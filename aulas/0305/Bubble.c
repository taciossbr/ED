/*
#include <stdio.h>

int main(){
	int i, n, j, aux;
	printf("Numero n: \n");
	scanf("%d", &n);
	int v[n];
	for(i=1; i<n; i++){ 	// numero de vezes que o laço vai fazer
		for(j=0; j<n-i; j++){ 	// laço de comparacação
			if(v[j]>v[j+1]){
				aux = v[j];
				v[j] = v[j+1];
				v[j+1] = aux;
			}
		}	
	}
	return 0;
}

*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
int main(){
	int m, n;
	
	srand(time(NULL)); //inicializa
	m = rand() %101; // valores de 0 a 100
	n = rand() %101;
	printf("\nm = %d\nn = %d\n", m, n);
	return 0;
}

*/

void gera_vetor(int * v, int n){
	int i;
	for(i=0; i<n; i++){
		v[i] = rand() % 101;
	}
}

void mostra_vetor(int * v, int n, char * msg){
	int i;
	printf("\n%s: \n", msg);
	for(i=0; i<n; i++){
		printf("%d ", v[i]);
	}
	printf("\n");
}

void bubble(int * v, int n){
	int i, j, aux;
	for(i=1; i<n; i++){
		for(j=0; j<n-i; j++){
			if(v[j] > v[j+1]){
				aux = v[j];
				v[j] = v[j+1];
				v[j+1] = aux;
			} 
		}	
	}
}

int busca_simples (int * v, int n, int x, int * cont){
//devolve a primeira posição de x ou -1, se não encontrar
	int i; 
	*cont = 0;
	for(i=0; i<n; i++){
		(*cont)++;
		if(v[i] == x){
			return i;
		}
	}
	return -1;
}

int busca_binaria (int * v, int n, int x, int * cont){
//devolve uma posição de x ou -1, se não encontrar
	int ini = 0, fim = n-1, meio;
	*cont = 0;
	while(ini <= fim){
		(*cont)++;
		meio = (ini + fim) / 2;
		if(x == v[meio]){
			return meio;
		}			
		if(x > v[meio]){
			ini = meio + 1;
		}
		else{
			fim = meio - 1;
		}
	}
	return - 1;
}

int main(){
	int *v, n, x, pos, cont;
	printf("Digite o tamanho do vetor: ");
	scanf("%d",&n);
	printf("Digite o valor de x: ");
	scanf("%d",&x);
	v = (int *)malloc(n * sizeof(int)); //vetor tamanho n (dinamico)
	srand(time(NULL));
	gera_vetor(v, n);
	mostra_vetor(v, n, "vetor gerador");
	pos = busca_simples(v, n, x, &cont);
	if(pos > -1){
		printf("\nbusca simples: elemento encontrado na posição %d\n",pos);		
		printf("número de comparações = %d\n", cont);
	}
	else{
		printf("\nbusca simples: elemento não encontrado\n");		
	}
	bubble(v, n);
	mostra_vetor(v, n, "vetor ordenado");	
	pos = busca_binaria(v, n, x, &cont);
	if(pos > -1){
		printf("\nbusca binaria: elemento encontrado na posição %d\n",pos);		
		printf("número de comparações = %d\n", cont);
	}
	else{
		printf("\nbusca binaria: elemento não encontrado\n");		
	}
	return 0;
}







