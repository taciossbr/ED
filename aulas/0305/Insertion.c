#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void insertion_sort(int * v, int n){
	int i, j, x;
	for(j=1; j<n; j++){
		x = v[j];
		for(i = j-1; i>=0 && v[i]>x; i--){
			v[i+1] = v[i];
		}
		v[i+1] = x;
	}
}
