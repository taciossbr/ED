#include <stdio.h>

void imp_cresc(int);

int main(){
    imp_cresc(12);
    return 0;
}

void imp_cresc(int n){
    if (n == 0){
        printf("0\n");
    } else {
        imp_cresc(n - 1);
        printf("%d\n", n);
    }
}