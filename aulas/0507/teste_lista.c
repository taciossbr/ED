#include "lista.h"
#include <stdio.h>

int main(){
    struct lista * l1 = cria_lista();
    int i;  
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if(insere_inicio(3, l1))
        printf("\n3 inserido no inicio com sucesso\n");
    if(insere_inicio(4, l1))
        printf("\n4 inserido no inicio com sucesso\n");
    if(insere_inicio(5, l1))
        printf("\n5 inserido no inicio com sucesso\n");	
    if(insere_fim(6, l1))
        printf("\n6 inserido no fim com sucesso\n");		
    if(insere_fim(7, l1))
        printf("\n7 inserido no fim com sucesso\n");		
    if(insere_fim(8, l1))
        printf("\n8 inserido no fim com sucesso\n");		
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if(remove_inicio(&i, l1))
        printf("\n%d removido no inicio com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if(remove_fim(&i, l1))
        printf("\n%d removido no fim com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (insere(9, 1, l1))
        printf("\n9 inserido da posição 1 com sucesso\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (insere(9, 5, l1))
        printf("\n9 inserido da posição 5 com sucesso\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (insere(9, 42, l1))
        printf("\n9 inserido da posição 5 com sucesso\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    struct lista *l2 = cria_lista();
    if (insere(9, 0, l2))
        printf("\n9 inserido da posição 0 com sucesso\n");
    mostra_lista(l2);
    printf("tamanho = %d\n\n", tamanho(l2));
    if (insere(3, 2, l2))
        printf("\n9 inserido da posição 2 com sucesso\n");
    mostra_lista(l2);
    printf("tamanho = %d\n\n", tamanho(l2));
    if (insere(3, 1, l2))
        printf("\n3 inserido da posição 1 com sucesso\n");
    mostra_lista(l2);
    printf("tamanho = %d\n\n", tamanho(l2));
    if (insere(4, 2, l2))
        printf("\n4 inserido da posição 2 com sucesso\n");
    mostra_lista(l2);
    printf("tamanho = %d\n\n", tamanho(l2));
    printf("\n\n");
    mostra_lista(l1);
    printf("\n\n");
    if (remove_pos(&i, 0, l1))
        printf("\n%d removido da posição 0 com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (remove_pos(&i, 1, l1))
        printf("\n%d removido da posição 1 com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (remove_pos(&i, 2, l1))
        printf("\n%d removido da posição 2 com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    if (remove_pos(&i, 2, l1))
        printf("\n%d removido da posição 2 com sucesso\n", i);
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));


    if (get(&i, 0, l1))
        printf("\n%d obtido da posição 0 com sucesso\n", i);
    else
        printf("\nimpossivel obter elemento na posição 0\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));

    if (get(&i, 1, l1))
        printf("\n%d obtido da posição 1 com sucesso\n", i);
    else
        printf("\nimpossivel obter elemento na posição 1\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));

    if (get(&i, 2, l1))
        printf("\n%d obtido da posição 2 com sucesso\n", i);
    else
        printf("\nimpossivel obter elemento na posição 2\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));


    if(insere_inicio(3, l1))
        printf("\n3 inserido no inicio com sucesso\n");
    if(insere_inicio(4, l1))
        printf("\n4 inserido no inicio com sucesso\n");
    if(insere_inicio(5, l1))
        printf("\n5 inserido no inicio com sucesso\n");	
    if(insere_fim(6, l1))
        printf("\n6 inserido no inicio com sucesso\n");		
    if(insere_fim(7, l1))
        printf("\n7 inserido no inicio com sucesso\n");		
    

    mostra_lista(l1);

    i = remove_x(5, l1);
    if (i != -1)
        printf("\n5 removido da posição %d com sucesso\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));

    i = remove_x(2, l1);
    if (i != -1)
        printf("\n2 removido da posição %d com sucesso\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    
    
    i = remove_x(7, l1);
    if (i != -1)
        printf("\n7 removido da posição %d com sucesso\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));


    i = remove_todos_x(7, l1);
    if (i)
        printf("\n7 removido %d vezes\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));
    
    
    i = remove_todos_x(4, l1);
    if (i)
        printf("\n4 removido %d vezes\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));


    i = remove_todos_x(6, l1);
    if (i)
        printf("\n6 removido %d vezes\n", i);
    else
        printf("\nelemento não encontrado\n");
    mostra_lista(l1);
    printf("tamanho = %d\n\n", tamanho(l1));


    struct lista *l3 = cria_lista();
    for(i = 8; i >= 0; i-=2){
        insere_inicio(i, l3);
    }
    mostra_lista(l3);

    i = insere_ordem(1, l3);
    if(i != -1)
        printf("\n1 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    i = insere_ordem(0, l3);
    if(i != -1)
        printf("\n0 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    i = insere_ordem(-1, l3);
    if(i != -1)
        printf("\n-1 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    i = insere_ordem(7, l3);
    if(i != -1)
        printf("\n7 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    i = insere_ordem(8, l3);
    if(i != -1)
        printf("\n8 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    i = insere_ordem(9, l3);
    if(i != -1)
        printf("\n9 inserido no inicio com sucesso na posição %d\n", i);
    mostra_lista(l3);

    printf("\n\n\n\n\n\n");
    struct lista *l4 = cria_lista();
    insere_inicio(4, l4);
    insere_inicio(3, l4);
    insere_inicio(2, l4);
    insere_inicio(2, l4);
    insere_inicio(5, l4);
    insere_inicio(6, l4);
    insere_inicio(2, l4);
    mostra_lista(l4);
    remove_todos_x(2, l4);
    mostra_lista(l4);

    printf("\n\n\n\n\n\n");
    struct lista *l5 = cria_lista();
    insere_inicio(4, l5);
    insere_inicio(3, l5);
    insere_inicio(2, l5);
    insere_inicio(2, l5);
    insere_inicio(5, l5);
    insere_inicio(6, l5);
    insere_inicio(2, l5);
    mostra_lista(l5);
    if (remove_pos(&i, 6, l5))
        puts("sucesso");
    mostra_lista(l5);


    return 0;
}
