#include "no.h"
#include<stdio.h>

struct lista{
	struct no * primeiro; //ponteiro de struct nó
};

struct lista * cria_lista();

int lista_vazia(struct lista *);

int insere_inicio(int, struct lista *);

void mostra_lista(struct lista *);

int insere_fim(int, struct lista *);

int remove_inicio(int *, struct lista *);

int remove_fim(int *, struct lista *);

// Funções de Exercícios

int tamanho(struct lista*);

int insere(int, int, struct lista*);
int remove_pos(int*, int, struct lista*);

int get(int *, int, struct lista *);
int remove_x(int, struct lista*);
int remove_todos_x(int, struct lista *);

int insere_ordem(int, struct lista*);