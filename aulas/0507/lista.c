#include "lista.h"

struct lista * cria_lista()
{
    struct lista * l = (struct lista *) malloc (sizeof(struct lista));
    if(l != NULL)
        l->primeiro = NULL;
    return l;
}

int lista_vazia(struct lista * l)
{
    if(l->primeiro == NULL)
        return 1;
    return 0;
    // return l->primeiro == NULL;
}

int insere_inicio(int i, struct lista * l)
{
    struct no * novo = cria_no(i);
    if(novo == NULL)
        return 0;
    /*if(lista_vazia(l)){
        l->primeiro = novo;		
    }
    else{
        novo->prox = l->primeiro;
        l->primeiro = novo;	
    }*/
    if(!lista_vazia(l))
        novo->prox = l-> primeiro;
    l->primeiro = novo;
    return 1;
}


void mostra_lista(struct lista * l)
{
    struct no * aux;
    if(lista_vazia(l))
        printf("\nlista vazia\n");
    else {
        aux = l->primeiro;
        while(aux != NULL) { //até o final da lista
            printf("%d ", aux->info);
            aux = aux->prox;
        }
        printf("\n");		
    }
}

int insere_fim(int i, struct lista * l)
{
    struct no * novo = cria_no(i);
    struct no * aux;
    if(novo == NULL)
        return 0;
    if(lista_vazia(l))
        l-> primeiro = novo;
    else {
        aux = l->primeiro;
        while(aux->prox != NULL) { //até o ultimo elemento
            aux = aux->prox;
        }
        aux->prox = novo;
    }
    return 1;
}

int remove_inicio(int * i, struct lista * l)
{
    struct no * aux;
    if(lista_vazia(l))
        return 0;
    aux = l->primeiro;
    *i = l->primeiro->info;
    l->primeiro = l->primeiro->prox;
    free(aux);
    return 1;
}

int remove_fim(int * i, struct lista * l)
{
    struct no * aux;
    if(lista_vazia(l))
        return 0;
    if(l->primeiro->prox == NULL) { //tem um elemento só(nó)
        *i = l->primeiro->info;
        free(l->primeiro);
        l->primeiro = NULL;
    } else {
        aux = l->primeiro;
        while(aux->prox->prox != NULL) { //até o penultimo elemento
            aux = aux->prox;
        }
        *i = aux->prox->info;
        free(aux->prox);
        aux->prox = NULL;
    }
    return 1;
}


int tamanho(struct lista *l)
{
    struct no * aux;
    if(lista_vazia(l))
        return 0;
    else {
        int c = 0;
        aux = l->primeiro;
        while(aux != NULL) { //até o final da lista
            c++;
            aux = aux->prox;
        }
        return c;	
    }
}

int insere(int x, int pos, struct lista *l)
{
    // novo na pos 1
    // a -> novo -> b -> c
    struct no *aux = l->primeiro;
    struct no *novo = cria_no(x);
    if(novo == NULL)
        return 0;
    int c = 0;
    if (pos == 0)
        return insere_inicio(x, l);
    while (aux != NULL) {
        if (c == pos - 1) {
            novo->prox = aux->prox;
            aux->prox = novo;
            return 1;
        }
        aux = aux->prox;
        c++;
    }
    return 0;
}

int remove_pos(int *x, int pos, struct lista *l)
{
    // remove na pos 1
    // a -> b -> c
    // a -> c
    struct no *aux = l->primeiro;
    int c = 0;
    if (pos == 0)
        return remove_inicio(x, l);
    while (aux->prox != NULL) {
        if (c == pos - 1) {
            *x = aux->prox->info;
            free(aux->prox);
            aux->prox = aux->prox->prox;
            return 1;
        }
        aux = aux->prox;
        c++;
    }
    return 0;
}

int get(int *x, int pos, struct lista *l)
{
    struct no *aux = l->primeiro;
    int c = 0;
    while (aux != NULL) {
        if (c == pos) {
            *x = aux->info;
            return 1;
        }
        aux = aux->prox;
        c++;
    }
    return 0;
}


int remove_x(int x, struct lista *l)
{
    struct no *aux = l->primeiro;
    int c = 0;
    if (aux->info == x) {
        free(l->primeiro);
        l->primeiro = aux->prox;
        return 0;
    }
    while (aux->prox != NULL) {
        if (aux->prox->info == x) {
            free(aux->prox);
            aux->prox = aux->prox->prox;
            return c;
        }
        aux = aux->prox;
        c++;
    }
    return -1;
}

int remove_todos_x(int x, struct lista *l)
{
    struct no *aux = l->primeiro;
    struct no *ant;
    int c = 0;
    if (aux->info == x) {
        free(l->primeiro);
        l->primeiro = aux->prox;
        ant = aux;
        aux = aux->prox;
        c++;
    }
    while (aux->prox != NULL) {
        if (aux->prox->info == x) {
            free(aux->prox);
            aux->prox = aux->prox->prox;
            c++;
        } else {
            ant = aux;
            aux = aux->prox;
        }
    }
    if (aux->info == x) {
        free(ant->prox);
        ant->prox = NULL;
    }
    return c;
}

int insere_ordem(int x, struct lista *l)
{
    // 1 -> 2 -> 4
    // 1 -> 2 -> 3 -> 4
    // 1 -> 2 -> 3 -> 3 -> 4
    struct no *aux = l->primeiro;
    if(x < aux->info) {
        insere_inicio(x, l);
        return 0;
    }
    int c = 1;
    struct no *novo = cria_no(x);
    if(novo == NULL)
        return -1;
    while (aux->prox != NULL) {
    // while (aux->prox != NULL) {
        if (x <= aux->prox->info) {
            novo->prox = aux->prox;
            aux->prox = novo;
            return c;
        }
        aux = aux->prox;
        c++;
    }
    if(x > aux->info) {
        aux->prox = novo;
        novo->prox = NULL;
        return c;
    }
    return -1;
}