#ifndef _ARVORE_H
#define _ARVORE_H

#include "no_bin.h"

typedef struct arvore {
    t_no_bin *raiz;
} t_arvore;

t_arvore * cria_arvore();

int arvore_vazia(const t_arvore *);

int insere(int, t_arvore *);

void percurso_em_ordem(t_arvore *);

#endif