#ifndef _EXERCICIOS_H
#define _EXERCICIOS_H

#include "arvore.h"

int mostrar_maior(int*, t_arvore *);

int mostrar_menor(int*, const t_arvore *);

int contar_nos(int *, const t_arvore*);

int contar_folhas(int*, const t_arvore*);

int verifica_elemento(int, const t_arvore*);

int contar_elemento(int,const t_arvore*);

void mostrar_arvore_inversa(const t_arvore*);

#endif