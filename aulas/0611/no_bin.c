#include <stdlib.h>
#include "no_bin.h"

t_no_bin * cria_no_bin(int i)
{
    t_no_bin *novo = (t_no_bin *) malloc(sizeof(t_no_bin));

    if(novo) {
        novo->info = i;
        novo->esq = NULL;
        novo->dir = NULL;
    }
    return novo;
}