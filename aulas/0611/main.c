#include "arvore.h"
#include "exercicios.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    int i,n;
    t_arvore * a = cria_arvore();
    srand(time(0));
    for(i = 1;i<=20;i++){
        n = rand()%10;
        printf("%d ",n);
        insere(n,a);
    }
    // system("pause >> NULL");
    printf("\n\n");
    percurso_em_ordem(a);

    mostrar_maior(&i, a);
    printf("maior valor: %d\n", i);
    
    mostrar_menor(&i, a);
    printf("menor valor: %d\n", i);

    contar_nos(&i, a);
    printf("qtd nos: %d\n", i);
    
    contar_folhas(&i, a);
    printf("qtd folhas: %d\n", i);

    mostrar_arvore_inversa(a);

    if(verifica_elemento(0, a)) {
        puts("0 esta na arvore");
    } else {
        puts("0 não esta na arvore");
    }
    if(verifica_elemento(5, a)) {
        puts("5 esta na arvore");
    } else {
        puts("5 não esta na arvore");
    }
    if(verifica_elemento(9, a)) {
        puts("9 esta na arvore\n");
    } else {
        puts("9 não esta na arvore\n");
    }

    i = contar_elemento(0, a);
    printf("0: %d\n", i);
    i = contar_elemento(5, a);
    printf("5: %d\n", i);
    i = contar_elemento(9, a);
    printf("9: %d\n", i);
    

    return 0;
}
