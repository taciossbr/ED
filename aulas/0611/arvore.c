#include <stdlib.h>
#include "arvore.h"
#include "no_bin.h"
#include <stdio.h>


t_arvore * cria_arvore()
{
    t_arvore * a = (t_arvore *) malloc(sizeof(t_arvore));
    if(a) {
        a->raiz = NULL;
    }
    return a;
}

int arvore_vazia(const t_arvore *a)
{
    // if(a->raiz != NULL) {
    //     return 1;
    // } else {
    //     return 0;
    // }

    // return (a->raiz != NULL);

    return !a->raiz;
}


void insere_aux(t_no_bin *novo, t_no_bin *atual)
{
    if(novo->info <= atual->info) {
        if(atual->esq == NULL) {
            atual->esq = novo;
        } else{
            insere_aux(novo, atual->esq);
        }
    } else {
        if(atual->dir == NULL) {
            atual->dir = novo;
        } else {
            insere_aux(novo, atual->dir);
        }
    }
    
}


int insere(int i, t_arvore *a)
{
    t_no_bin * novo = cria_no_bin(i);
    if(novo) {
        if(arvore_vazia(a)) {
            a->raiz = novo;
        } else {
            insere_aux(novo, a->raiz);
        }
        return 1;
    }
    return 0;
}


void percurso_aux(t_no_bin * atual)
{
    if(atual) { // diferente de NULL
        percurso_aux(atual->esq);
        printf("%d ",atual->info);
        percurso_aux(atual->dir);
    }
}


void percurso_em_ordem(t_arvore *a)
{

    if(arvore_vazia(a)) {
        printf("\arvore vazia\n");
    } else {
        percurso_aux(a->raiz);
        printf("\n\n");
    }
}

