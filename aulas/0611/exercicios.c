#include <stdlib.h>
#include <stdio.h>
#include "arvore.h"
#include "no_bin.h"
#include "exercicios.h"

int mostrar_maior_aux(t_no_bin *atual)
{
    if(atual->dir != NULL) {
        return mostrar_maior_aux(atual->dir);
    }
    return atual->info;
}



int mostrar_maior(int *i, t_arvore *a)
{
    if(arvore_vazia(a)) {
        return 0;
    }
    *i = mostrar_maior_aux(a->raiz);
    return 1;
}

int mostrar_menor_aux(t_no_bin *atual)
{
    if(atual->esq != NULL) {
        return mostrar_menor_aux(atual->esq);
    }
    return atual->info;
}

int mostrar_menor(int *i, const t_arvore *a)
{
    if(arvore_vazia(a)) {
        return 0;
    }
    *i = mostrar_menor_aux(a->raiz);
    return 1;
}

int contar_nos_aux(const t_no_bin *no) {

    if(no == NULL) {
        return 0;
    }
    int n = contar_nos_aux(no->dir) + 1
            + contar_nos_aux(no->esq);
    return n;
    
}

int contar_nos(int *i, const t_arvore *a)
{
    if(arvore_vazia(a)) {
        *i = 0;
        return 0;
    }
    *i = contar_nos_aux(a->raiz);
    return 1;
}

int contar_folhas_aux(const t_no_bin *no) {

    int n = 0;

    if(no->dir == NULL && no->esq == NULL) {
        return 1;
    }

    if(no->dir != NULL) {
        n += contar_folhas_aux(no->dir);
    }

    if(no->esq != NULL) {
        n += contar_folhas_aux(no->esq);
    }

    return n;
    
}

int contar_folhas(int *i, const t_arvore *a)
{
    if(arvore_vazia(a)) {
        *i = 0;
        return 0;
    }
    *i = contar_folhas_aux(a->raiz);
    return 1;
}



void mostrar_arvore_inversa_aux(t_no_bin * atual)
{
    if(atual) { // diferente de NULL
        mostrar_arvore_inversa_aux(atual->dir);
        printf("%d ",atual->info);
        mostrar_arvore_inversa_aux(atual->esq);
    }
}

void mostrar_arvore_inversa(const t_arvore *a)
{
    if(arvore_vazia(a)) {
        printf("\arvore vazia\n");
    } else {
        mostrar_arvore_inversa_aux(a->raiz);
        printf("\n\n");
    }
}

int verifica_elemento_aux(int i, const t_no_bin *atual)
{
    if(atual == NULL) {
        return 0;
    }
    if(i == atual->info) {
        return 1;
    } else if(i < atual->info) {
        return verifica_elemento_aux(i, atual->esq);
    } else {
        return verifica_elemento_aux(i, atual->dir);
    }
}

int verifica_elemento(int i, const t_arvore *a)
{
    if(arvore_vazia(a)) {
        return 0;
    } else {
        return verifica_elemento_aux(i, a->raiz);
    }
}

int contar_elemento_aux(int i, const t_no_bin *atual)
{
    if(atual == NULL) {
        return 0;
    }
    int n = 0;
    if(i == atual->info) {
        n = 1 + contar_elemento_aux(i, atual->esq);
    } else if(i <= atual->info) {
        n = contar_elemento_aux(i, atual->esq);
    } else {
        n = contar_elemento_aux(i, atual->dir);
    }
    return n;
}

int contar_elemento(int i, const t_arvore *a)
{
    if(arvore_vazia(a)) {
        return 0;
    } else {
        return contar_elemento_aux(i, a->raiz);
    }
}