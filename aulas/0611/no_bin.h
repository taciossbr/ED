#ifndef _NO_BIN_H
#define _NO_BIN_H

typedef struct no_bin {
    int info;
    struct no_bin *esq;
    struct no_bin *dir;
} t_no_bin;

t_no_bin * cria_no_bin(int);

#endif