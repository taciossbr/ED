#include <stdio.h>

int fat (int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * fat (n - 1);
}
int fib (int n) {
    if (n == 0 || n == 1)
        return 1;
    return fib(n - 1) + fib(n - 2);
}

int main (void) {
    int i;
    // for (i = 0; i <= 20; i++)
    //     printf("fatorial de %d = %d\n", i, fat(i));
    for (i = 0; i <= 45; i++)
        printf("fibonacci de %d = %d\n", i, fib(i));
    
    return 0;
}