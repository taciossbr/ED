#ifndef _FILA_H
#define _FILA_H

#include <stdio.h>
#include <stdlib.h>
typedef struct fila Fila;

Fila *cria_fila();

int fila_vazia(const Fila *);
int fila_cheia(const Fila *);

int insere(int, Fila *);
int remov(int *, Fila *);

int tamanho(const Fila *);

void mostra_fila(const Fila *);

#endif