#include <stdio.h>
#include "fila.h"
int main(void)
{
    Fila *f = cria_fila();
    if (fila_vazia(f))
        puts("A Fila esta vazia");
    insere(9, f);
    mostra_fila(f);
    if (fila_vazia(f))
        puts("A Fila esta vazia");
    else
        puts("A Fila não esta vazia");
    insere(8, f);
    mostra_fila(f);
    insere(7, f);
    mostra_fila(f);
    insere(6, f);
    mostra_fila(f);
    if (fila_cheia(f))
        puts("A Fila esta cheia");
    else
        puts("A Fila não esta cheia");

    if (insere(3,f))
        puts("3 inserido con sucesso :)");
    else
        puts("deu ruim :(");
    mostra_fila(f);

    int i;
    if(remov(&i, f))
        printf("%d removido com sucesso :)\n", i);
    else
        puts("deu ruim :(");
    mostra_fila(f);

    if (fila_cheia(f))
        puts("A Fila esta cheia");
    else
        puts("A Fila não esta cheia");

    if(remov(&i, f))
        printf("%d removido com sucesso :)\n", i);
    else
        puts("deu ruim :(");
    mostra_fila(f);
    if(remov(&i, f))
        printf("%d removido com sucesso :)\n", i);
    else
        puts("deu ruim :(");
    mostra_fila(f);
    if(remov(&i, f))
        printf("%d removido com sucesso :)\n", i);
    else
        puts("deu ruim :(");
    mostra_fila(f);

    if (fila_vazia(f))
        puts("A Fila esta vazia");
    else
        puts("A Fila não esta vazia");

    if (insere(0,f))
        puts("0 inserido con sucesso :)");
    else
        puts("deu ruim :(");
    mostra_fila(f);
    insere(1,f);
    insere(2,f);
    insere(3,f);
    mostra_fila(f);

    if (fila_cheia(f))
        puts("A Fila esta cheia");
    else
        puts("A Fila não esta cheia");

    if(remov(&i, f))
        printf("%d removido com sucesso :)\n", i);
    else
        puts("deu ruim :(");
    mostra_fila(f);
    insere(4,f);
    mostra_fila(f);
    insere(5,f);
    mostra_fila(f);

    if (fila_cheia(f))
        puts("A Fila esta cheia");
    else
        puts("A Fila não esta cheia");

    return 0;
}