#include "fila.h"

struct fila {
    int primeiro, ultimo, cont, capacidade;
    int *dados;
};

Fila *cria_fila()
{
    int capacidade;
    Fila *f = (Fila *) malloc(sizeof(Fila));
    if(f != NULL) {
        printf("Digite a capacidade da fila: ");
        scanf(" %d", &capacidade);
        f->primeiro = f->ultimo = f->cont = 0;
        f->dados = (int *) malloc(sizeof(int) * capacidade);
        f->capacidade = capacidade;
    }
    return f;
}
int fila_vazia(const Fila *f)
{
    return (f->cont == 0);
}
int fila_cheia(const Fila *f) 
{
    return (f->cont == f->capacidade);
}
int insere(int e, Fila *f)
{
    if(fila_cheia(f))
        return 0;
    f->dados[f->ultimo] = e;
    f->ultimo = (f->ultimo+1) % f->capacidade;
    f->cont++;
    return 1;
}
int remov(int *e, Fila *f)
{
    if(fila_vazia(f))
        return 0;
    *e = f->dados[f->primeiro];
    f->primeiro = (f->primeiro + 1) % f->capacidade;
    f->cont--;
}

int tamanho(const Fila *f)
{
    return f->cont;
}

void mostra_fila(const Fila *f)
{
    int i, c;
    for(i = f->primeiro, c = 0; c < f->cont; i++, c++) {
        printf("%d ", f->dados[i%f->capacidade]);
    }
    printf("p=%d,u=%d,c=%d\n", f->primeiro, f->ultimo, f->cont);
}