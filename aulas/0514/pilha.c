#include <stdlib.h>
#include <stdio.h>
#include "pilha.h"
#include "no.h"

struct pilha {
    struct no *topo;
    int count;
};

Pilha* cria_pilha()
{
    Pilha *p = (Pilha *) malloc (sizeof(Pilha));
    if(p != NULL){
        p->topo = NULL;
        p->count = 0;
    }
    return p;
}

int pilha_vazia(const Pilha *p)
{
    /* if(p->topo == NULL)
        return 1;
    return 0; */
    // return !p->topo;
    return (p->topo == NULL);
}
int pilha_cheia(const Pilha *p)
{
    return 0;
}

int push(int i, Pilha *p)
{
    struct no * novo= cria_no(i);
    if(novo == NULL)
        return 0;
    if(!pilha_vazia(p))
        novo->prox = p->topo;
    p->topo = novo;
    p->count++;
    return 1;


}

int pop(int *i, Pilha *p)
{
    if(pilha_vazia(p))
        return 0;
    struct no *aux = p->topo;
    *i = aux->info;
    p->topo = p->topo->prox;
    p->count--;
    free(aux);
    return 1;
}

int tamanho_pilha( const Pilha *p)
{
    return p->count;
}

void mostra_pilha(const Pilha *p)
{
    struct no * aux;
    if(pilha_vazia(p))
        printf("\npilha vazia\n");
    else {
        aux = p->topo;
        while(aux != NULL) { //até o final da pilha
            printf("%d ", aux->info);
            aux = aux->prox;
        }
        printf("\n");		
    }
}