#ifndef _PILHA_H
#define MAX 10

typedef struct pilha Pilha;

Pilha * cria_pilha();

int push (int, Pilha *);

int pop (int *, Pilha *);

int pilha_cheia (const Pilha *);

int pilha_vazia (const Pilha *);

int tamanho_pilha (const Pilha *);

void mostra_pilha (const Pilha *);

int mostra_topo (const Pilha *);

#endif