#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"
struct pilha {
    int v[MAX];
    int topo;
};

Pilha * cria_pilha (){
    Pilha *p = (Pilha *) malloc (sizeof(Pilha));
    if (p){
        p->topo = 0;
    }
    return p; 
}

int push (int i, Pilha *p) {
    if (pilha_cheia(p))
        return 0;
    // p->v[p->topo] = i;
    // p->topo++;
    p->v[p->topo++] = i;
    return 1;
}

int pop (int *i, Pilha *p) {
    if (pilha_vazia(p))
        return 0;
    // p->topo--;
    // *i = p->v[p->topo];
    *i = p->v[--p->topo];
    return 1;
}

int pilha_cheia (const Pilha *p) {
    return (p->topo == MAX);
}
int pilha_vazia (const Pilha *p) {
    // return (p->topo == 0);
    return !p->topo;
}

int tamanho_pilha (const Pilha *p) {
    return p->topo;
}

void mostra_pilha (const Pilha *p) {
    if (pilha_vazia(p)){
        printf("\npilha vazia\n");
    } else {
        int i;
        printf("\n");
        for(i = p->topo-1; i >= 0; i--){
            printf("%d ", p->v[i]);
        }
        printf("\n");
    }
}

int mostra_topo (const Pilha *p) {
    if (pilha_vazia(p)) {
        return NULL;
    }
    return p->v[p->topo-1];
}