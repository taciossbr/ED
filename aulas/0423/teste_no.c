#include <stdio.h>
#include "no.h"

int main() {
    struct no no1, no2;
    struct no * ap_no1, ap_no2;
    printf ("tamanho do no: %d\n", sizeof(no1));
    printf ("tamanho do ap_no: %d\n", sizeof(ap_no1));
    printf ("onde esta o no1: %p\n", &no1);
    printf ("onde esta o no2: %p\n", &no2);
    printf ("onde esta o ponteiro1: %p\n", &ap_no1);
    printf ("onde esta o ponteiro2: %p\n", &ap_no2);

    no1.info = 37;
    no1.prox = &no2;

    ap_no1 = (struct no *) malloc(sizeof(struct no));
    ap_no1->info = 46;
    ap_no1->prox = &no1;
    
    ap_no1->prox->info = 100;

    return 0;
}