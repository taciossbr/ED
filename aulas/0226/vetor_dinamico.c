#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int *v;
    int capacidade, ocupacao;
} vetor_dinamico;

vetor_dinamico* cria_vetor (int c) {
    vetor_dinamico *din;
    // aloca o espaço inicial para a est. vetor dinamico
    din = (vetor_dinamico*) malloc(sizeof(vetor_dinamico));

    // aloca espaço para o vetor, seta capacidade e 
    // quantidade ocupada
    din->v = (int*) malloc(c*sizeof(int)); 
    din->capacidade = c;
    din->ocupacao = 0;

    return din;
}

int esta_cheio(vetor_dinamico *din){
    return din->ocupacao == din->capacidade;
}
int esta_vazio(vetor_dinamico *din){
    return (!din->ocupacao);
}

/* int diminui (vetor_dinamico *din) {
    // aloca um novo espaco de memoria com o dobro da capacidade
    int *aux = (int*) malloc(sizeof(int) * (din->capacidade / 2)); 
    if (aux == NULL) { // verifica se foi possivel alocar o novo espaço de memoria
        return 0; // se não foi p
    } else {
        int i;
        for (i = 0; i < din->ocupacao; i++){
            aux[i] = din->v[i];
        }
        free(din->v); // primeiro voce libera o conteudoantigo
        din->v = aux; // depois voce troca o valor da referencia
        din->capacidade /= 2; // divide o valor da capacidade pela metade
        return 1;
    }
}*/

/* int aumenta (vetor_dinamico *din) {
    // aloca um novo espaco de memoria com o dobro da capacidade
    int *aux = (int*) malloc(sizeof(int) * 2 * din->capacidade); 
    if (aux == NULL) { // verifica se foi possivel alocar o novo espaço de memoria
        return 0; // se não foi p
    } else {
        int i;
        for (i = 0; i < din->ocupacao; i++){
            aux[i] = din->v[i];
        }
        free(din->v); // primeiro voce libera o conteudoantigo
        din->v = aux; // depois voce troca o valor da referencia
        din->capacidade *= 2; // dobra o valor da capacidade
        return 1;
    }
} */

int redimenciona (vetor_dinamico *din, int nova) {
    // aloca um novo espaco de memoria com o dobro da capacidade
    int *aux = (int*) malloc(sizeof(int) * nova); 
    if (aux == NULL) { // verifica se foi possivel alocar o novo espaço de memoria
        return 0; // se não foi possivel retorna falso
    } else {
        // se foi possivel, realiza a copia do conteudo
        // para o novo espaço na memoria
        int i;
        for (i = 0; i < din->ocupacao; i++){
            aux[i] = din->v[i];
        }
        free(din->v); // primeiro voce libera o conteudoantigo
        din->v = aux; // depois voce troca o valor da referencia
        din->capacidade = nova; // divide o valor da capacidade pela metade
        return 1;
    }
}

int insere(int i, vetor_dinamico *din){
    if (esta_cheio(din))    // veirfica se esta cheio
        // se esta cheio, verifica se e possivel redimensionar
        if(!redimenciona(din, 2*din->capacidade))
            return 0;   // se nao foi possivel retorna falso
    // adiciona o elemento e incrementa o tamanho ocupado
    din->v[din->ocupacao++] = i;
    return 1;   // retorna true

}

int precisa_diminuir(vetor_dinamico *din){
    return din->ocupacao <= (float)din->capacidade/4;
}

int remove_din(int *i, vetor_dinamico *din){
    if(esta_vazio(din)) // verifica se esta cheio
        return 0; // retorna falso
    // remove o elemento e decrementa o tamanho do vetor
    *i = din->v[--din->ocupacao]; 
    if(precisa_diminuir(din)){ // verifica se precisa diminuir
        if(!redimenciona(din, din->capacidade/2))  
            return 1;
        return 0;
    }
    return 2;
}

void mostra_din(vetor_dinamico *din, char *s){
    int i;
    printf("\n%s\n", s);
    printf("capacidade = %d\n", din->capacidade);
    printf("ocupação = %d\n\n", din->ocupacao);

    for (i = 0; i<din->ocupacao; i++){
        printf("%d ", din->v[i]);
    }
    puts("");
}

int main(void){
    vetor_dinamico *v_din = cria_vetor(3);
    int i;

    puts("\n----------\nEnchendo\n-----------\n");
    for(i=1;i <=20; i++){
        insere(i%10, v_din);
        mostra_din(v_din, "");
    }
    puts("\n-----------\nEsvaziando\n-----------\n");
    while(!esta_vazio(v_din)){
        remove_din(&i, v_din);
        mostra_din(v_din, "");
    }
    return 0;
}