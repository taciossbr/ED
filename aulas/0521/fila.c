#include "fila.h"

struct fila {
    int primeiro, ultimo, cont, capacidade;
    int *dados;
};

Fila *cria_fila()
{
    int capacidade;
    Fila *f = (Fila *) malloc(sizeof(Fila));
    if(f != NULL) {
        printf("Digite a capacidade da fila: ");
        scanf(" %d", &capacidade);
        f->primeiro = f->ultimo = f->cont = 0;
        f->dados = (int *) malloc(sizeof(int) * capacidade);
        f->capacidade = capacidade;
    }
    return f;
}

int proxima(int pos, const Fila *f)
{
    return (pos + 1) % f->capacidade;
}

int fila_vazia(const Fila *f)
{
    // if(f->cont == 0)
    //     return 1;
    // return 0;

    // return (f->cont == 0);

    return !f->cont;
}

int fila_cheia(const Fila *f) 
{
    // if(f->cont == f->capacidade)
    //     return 1;
    // return 0;

    return (f->cont == f->capacidade);
}

int insere(int i, Fila *f)
{
    if(!fila_cheia(f)) {
        f->dados[f->ultimo] = i;
        f->ultimo = proxima(f->ultimo,f);
        f->cont++;
        return 1;
    }
    return 0;
}

int remov(int *i, Fila *f)
{
    if(!fila_vazia(f)) {
        *i = f->dados[f->primeiro];
        f->primeiro = proxima(f->primeiro,f);
        f->cont--;
        return 1;
    }
    return 0;
}

int tamanho(const Fila *f)
{
    return f->cont;
}

void mostra_fila(const Fila *f)
{
    int i;
    if(fila_vazia(f)) {
        printf("esta vazia");
    } else {
        for(i = f->primeiro; i != f->ultimo; i=proxima(i, f)) {
            printf("%d ", f->dados[i]);
        }
    }
    printf("\n");
}

void mostra_vetor(const Fila *f)
{
    // int i;

    // if (fila_cheia(f)) {
    //     for(i = 0; i < f->primeiro; i++){
    //         printf("%d ", f->dados[i]);
    //     }
    // }
    // else if (f->primeiro <= f->ultimo) {
    //     for(i = 0; i < f->primeiro; i++){
    //         printf("_ ");
    //     }
    //     for(i = f->primeiro; i != f->ultimo; i++){
    //         printf("%d ", f->dados[i]);
    //     }
    //     for(i = 0; i < f->capacidade; i++){
    //         printf("_ ");
    //     }
    // } else {
    //     for(i = 0; i< f->ultimo; i++){
    //         printf("%d ", f->dados[i]);
    //     }
    //     for(i = f->ultimo; i != f->primeiro; i++){
    //         printf("_ ");
    //     }
    //     for(i = f->primeiro; i < f->capacidade; i++){
    //         printf("%d ", f->dados[i]);
    //     }
    // }
    // printf("\n");
    int i;
    if(fila_vazia(f)) {
        for(i = 0; i < f->capacidade; i++){
            printf("_ ");
        }
    } else {
        for(i = 0; i < f->capacidade; i++) {
            if (f->primeiro < f->ultimo) {
                if (f->primeiro <= i && i < f->ultimo) {
                    printf("%d ", f->dados[i]);
                } else {
                    printf("_ ");
                }
            } else {
                if (f->ultimo <= i && i < f->primeiro) {
                    printf("%d ", f->dados[i]);
                } else {
                    printf("_ ");
                }
            }
        }
    }
    printf("\n");
}